import 'package:dimy_test/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(AppController());
    final TextStyle ingredientsStyle =
        TextStyle(color: Colors.white, fontSize: 12);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.brown, //change your color here
        ),
        title: const Text(
          "Home",
          style: TextStyle(color: Colors.brown),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(() {
              return controller.loading.value
                  ? Center(
                      child: const CircularProgressIndicator(),
                    )
                  : Container(
                      height: 100,
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.getCoffee.length,
                          itemBuilder: (context, index) {
                            var item = controller.getCoffee[index];
                            return Container(
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                              decoration: BoxDecoration(
                                color: Colors.brown[100],
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    item.title,
                                    style: const TextStyle(
                                        color: Colors.brown,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15),
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  ReadMoreText(
                                    item.description,
                                    trimLength: 100,
                                    colorClickableText: Colors.black,
                                    style: const TextStyle(
                                        color: Colors.brown,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                    trimMode: TrimMode.Length,
                                    trimCollapsedText: '\nShow more',
                                    trimExpandedText: '\nShow less',
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Container(
                                    // width: 50,
                                    height: 30,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: item.ingredients.length,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          var itemingredients =
                                              item.ingredients[index];
                                          Size txtSize = _textSize(
                                              itemingredients,
                                              ingredientsStyle);
                                          return Container(
                                            alignment: Alignment.center,
                                            width: txtSize.width + 20,
                                            // padding: const EdgeInsets.all(5),
                                            margin:
                                                const EdgeInsets.only(right: 5),
                                            decoration: BoxDecoration(
                                              color: Colors.brown,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: Text(itemingredients,
                                                style: ingredientsStyle),
                                          );
                                          // return ListTile(
                                          //   title: Text(itemingredients),
                                          // );
                                        }),
                                  )
                                ],
                              ),
                            );
                          }),
                    );
            }),
          ),
        ],
      ),
    );
  }

  Size _textSize(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }
}
