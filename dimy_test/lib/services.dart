import 'dart:async';
import 'package:dimy_test/coffee_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:get/get.dart';

class Services {
  Future<List<CoffeeModel>?> getallpost() async {
    try {
      var response = await http
          .get(Uri.parse("https://api.sampleapis.com/coffee/hot"))
          .timeout(const Duration(seconds: 10), onTimeout: () {
        throw TimeoutException("Connection Time Out, Try again");
      });
      if (response.statusCode == 200) {
        List jsonresponse = convert.jsonDecode(response.body);
        List<CoffeeModel> parsedResponse =
            jsonresponse.map((e) => CoffeeModel.fromJson(e)).toList();
        parsedResponse.sort((a, b) => a.id.compareTo(b.id));
        return parsedResponse;
      } else {
        return null;
      }
    } catch (e) {
      Get.snackbar(
        "Failed to get data",
        "error message",
        backgroundColor: Colors.grey,
        snackPosition: SnackPosition.BOTTOM,
        messageText: Text(
          e.toString(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      );
    }
  }
}
