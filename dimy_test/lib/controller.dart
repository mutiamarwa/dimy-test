import 'package:dimy_test/coffee_model.dart';
import 'package:dimy_test/services.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  var getCoffee = <CoffeeModel>[].obs;
  Services services = Services();
  var loading = true.obs;
  @override
  void onInit() {
    callpostmethod();
    super.onInit();
  }

  callpostmethod() async {
    try {
      loading.value = true;
      var result = await services.getallpost();
      if (result != null) {
        getCoffee.assignAll(result);
      } else {
        print("null");
      }
    } finally {
      loading.value = false;
    }
    update();
  }
}
