import 'package:dimy_test/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    TextEditingController _pinController = TextEditingController();
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text(
      //     "Login",
      //     style: TextStyle(color: Colors.brown),
      //   ),
      //   backgroundColor: Colors.transparent,
      //   elevation: 0,
      //   centerTitle: true,
      //   systemOverlayStyle: SystemUiOverlayStyle.dark,
      // ),
      body: Container(
        padding: const EdgeInsets.all(15),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Coffee Cheat Sheet",
                style: TextStyle(
                    color: Colors.brown,
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                  padding: const EdgeInsets.only(left: 40),
                  height: 200,
                  child: Image.asset("assets/images/logo2.png")),
              const SizedBox(
                height: 30,
              ),
              Container(
                // height: 70,
                padding: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: _pinController,
                  validator: validatePin,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 12.0, horizontal: 12.0),
                    border: const OutlineInputBorder(),
                    hintText: "Enter PIN",
                    labelText: "PIN",
                    labelStyle:
                        TextStyle(fontSize: 14, color: Colors.grey[700]),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: 150,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  style: raisedButtonStyle_dark,
                  child: const Text("Login"),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Get.to(() => const HomePage());
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

String? validatePin(String? formPin) {
  if (formPin == null || formPin.isEmpty) return 'Harap masukan PIN';

  // String pattern = r'^(?=.*[0-9]).{4,}$';
  String pattern = r'^-?[0-9]+$';
  RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(formPin)) return '''PIN harus berupa angka''';

  if (formPin == '2022') {
    return null;
  } else {
    return 'PIN salah, harap coba kembali';
  }
}

final ButtonStyle raisedButtonStyle_dark = ElevatedButton.styleFrom(
  onPrimary: Colors.white,
  primary: Colors.brown,
  // minimumSize: Size(88, 36),
  minimumSize: const Size.fromHeight(50),
  padding: const EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
