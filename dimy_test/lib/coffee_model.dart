// To parse this JSON data, do
//
//     final coffeeModel = coffeeModelFromJson(jsonString);

import 'dart:convert';

List<CoffeeModel> coffeeModelFromJson(String str) => List<CoffeeModel>.from(
    json.decode(str).map((x) => CoffeeModel.fromJson(x)));

String coffeeModelToJson(List<CoffeeModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CoffeeModel {
  CoffeeModel({
    required this.title,
    required this.description,
    required this.ingredients,
    required this.id,
  });

  String title;
  String description;
  List<String> ingredients;
  int id;

  factory CoffeeModel.fromJson(Map<String, dynamic> json) => CoffeeModel(
        title: json["title"],
        description: json["description"],
        ingredients: List<String>.from(json["ingredients"].map((x) => x)),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "description": description,
        "ingredients": List<dynamic>.from(ingredients.map((x) => x)),
        "id": id,
      };
}
